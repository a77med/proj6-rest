# Laptop Service


from flask_restful import Resource, Api
import flask
import os
from flask import Flask, request, Response                                                                                                                                 
import pymongo
from pymongo import MongoClient
import csv

###                                                                                                                                                                                                   
# Globals                                                                                                                                                                                             
###                                                                                                                                                                                                  # Instantiate the app                                                                                                                                                                                 
app = Flask(__name__)
api = Api(app) 

#CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY

#Creating a client object                                                                                                                                                                             
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

#Connecting to the db                                                                                                                                                                                 
db = client.timesdb



class listAll(Resource):
    def get(self):
        # if there is a top requested, get the integer
        top = request.args.get('top')
        # if there  is a string, return it as an int
        if top != None:
            top = int(top)
        # Basically unlimited, given our limited table
        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from
        # the Mongo DB
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists
        for item in item_list:
            open_time.append(item["open_time"])
            close_time.append(item["close_time"])

        # Adding key and values
        time_dict ["open_time"] = open_time
        # Adding key and values
        time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                   
api.add_resource(listAll, '/listAll')



class listOpenOnly(Resource):
    def get(self):
	# if there is a top requested, get the integer
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                          
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
           

	# Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        #time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnly, '/listOpenOnly')




class listCloseOnly(Resource):
    def get(self):
	# if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            #open_time.append(item["open_time"])
            close_time.append(item["close_time"])

	# Adding key and values                                                                                                                                                                      
        #time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                   
        time_dict ["close_time"] = close_time
        
        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnly, '/listCloseOnly')



class listAllcsv(Resource):
    def get(self):
	# if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                    
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow([["Open Time"],["Close Time"]])

        for item in item_list:
            csv_write.writerow([item["open_time"], item["close_time"]])

	# Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listAllcsv, '/listAll/csv')


class listOpenOnlycsv(Resource):
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')

        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)

        else:
            top = 100

        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)

        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                       
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow(["Open Time"])

        for item in item_list:
            csv_write.writerow([item["open_time"]])

        # Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")
        
# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
        
        
class listCloseOnlycsv(Resource):
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
    
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
        
            # Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        
        # Empty dict and lists (to be appended)                                                                                                                                                       
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow(["Close Time"])

        for item in item_list:
            csv_write.writerow([item["close_time"]])

        # Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')




class listAlljson(Resource):
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                                
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)

        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                               
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []
        
        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
            close_time.append(item["close_time"])
            
        # Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        time_dict ["close_time"] = close_time
        
        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listAlljson, '/listAll/json')


class listOpenOnlyjson(Resource):
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
        else: 
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
            #close_time.append(item["close_time"])

        # Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        #time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnlyjson, '/listOpenOnly/json')


class listCloseOnlyjson(Resource):
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                                
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            # Basically unlimited, given our limited table                                                                                                                                    
        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                               
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            #open_time.append(item["open_time"])
            close_time.append(item["close_time"])

        # Adding key and values                                                                                                                                                                      
        #time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnlyjson, '/listCloseOnly/json')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
