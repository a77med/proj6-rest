# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## What is in this repository

The implementations have been added and tested successfully. To make sure it is explained:

on localhost:5000, index will show list all times, list open only, and list close only.
To access the php format of listAll, etc. you have to use localhost:5000/listAll.php as such.

on localhost:5001, listAll, listOpenOnly, listCloseOnly, listAll/csv, listOpenOnly/csv, listCloseOnly/csv,
listAll/json, listOpenOnly/json, listCloseOnly/json, are all implemented, and will be printed depending on
the k in ?top=k if it exists. K will manipulate the result by printing the first K elements.

On localhost:5003, Resides our acp_times calculator that calculates the timings, including
submitting data, and displaying data from assignment 5.

localhost:5002 is our mongodb.

Everything should be working fine when running fine when composed through docker.

However, the expectation of the instructor for the service at localhost:5000 is unclear.
